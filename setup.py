#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from setuptools import setup
import evildict

setup(name='evildict',
      author=evildict.__author__,
      author_email='nwillhite@isilon.com',
      version=open('VERSION.txt').read(),
      packages=['evildict'],
      scripts=['bin/evilserver.py',],
      description='NOT FOR PRODUCTION: A thread safe caching tree data structure',
      install_requires=['wheel', 'Flask'],
      license='LICENSE.txt',
     )
