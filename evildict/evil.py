# -*- coding: UTF-8 -*-

from collections import deque

from treedict import TreeDict 
from superlocks import ReadWriteLock

class Singleton(type):
    """
    Prevents multiple occurances of a class being created.
    When the constructor runs, if an instance of the class
    deemed to be a signleton has already been created, a
    referrence to that object is returned.

    This implamentation of a singleton in python is intended
    to be used as a metaclass.
    To make a class a singleton, simply add the following as
    a class attribute (normally right after your doc string):

        __metaclass__ = Singleton
    """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class EvilDict(object):
    """
    This is a thread safe caching dictionary class, which utilizes
    a Read-Write Lock (aka shared lock). This class is ideal for
    heavly nested dictonaries because lookups occur at O(n) time.
    By caching the lookup, we can reduce the time complexity to
    O(log(n)).

    @param dictionary
        The backend data structure that's read and written to.

        Type - dict

    @param timeout
        The number of seconds a thread will wait for
        a lock before raising a RuntimeError.

        Type - int

    @param cache_count
        The max count of items that can be cached.

        Type - int

    -Basic caching strategy information-
      This class follows a FIFO pattern, where multiple entries
      for the same cached item can exist. This is because reads can
      add to the cache, and we allow multiple readers at the same
      time. When the cache counter is beyond the defined limit, the
      oldest cached entry is removed.
 
      When a lookup is cached pointers to the parent tree are moved
      to the root; this means that subsequent lookups in that node do
      not require a full tree walk *before* we search the node itself.
      It's worth noting that because we are simply removing the need
      for a second tree walk, and that looking through the cache is
      faster than walking the tree, there is potental to reduce
      performance by making the cache very large.
    """

    __metaclass__ = Singleton

    def __init__(self, dictionary, timeout=30, cache_count=1000):
        self._lock = ReadWriteLock(read_timeout=timeout,
                                   write_timeout=timeout)
        self._cache_meta = deque()
        self._cache_max = cache_count
        self._tree = TreeDict(dictionary)

    def __len__(self):
        """
        A rought measure of the number of keys within EvilDict.
        """
        # minus one b/c the last item in the array is all the closing
        # braces ->  }}}}}}}}}}
        return len(self._tree.__str__().split(':')) - 1

    def _read(self, key):
        """
        Abstracts reading from the tree so both a read call and a modify
        call can perform the same type of read.

        *NOT THREAD SAFE*
        """
        try:
            root, cache_attr = self._parse_key(key)
            # tmp should have a subtree made of a dict
            tmp = getattr(self, cache_attr)
            answer = tmp.get(root[-1])
            if answer is None:
                # then our node is a root node, so tmp == the subtree answer
                answer = tmp
            return answer

        except AttributeError as e:
            #Try to add the lookup to the cache
            try:
                # get_node raises a KeyError if the node doesn't exist
                answer = self._tree.get_node(key)
                self._update_cache(cache_attr, cache_attr.replace('_', '.'))
                return answer

            except KeyError:
                raise LookupError('Key does not exist in tree: %s' % key)       
        except KeyError:
            raise LookupError('Invalid node %s for subtree %s' % (root[-1],
                              cache_attr.replace('_', '.')))


    def read(self, key):
        """
        A thread safe way to read the underlying tree data structure.
        Utilizes a lock that supports many readers at the same time.

        *Returns*
            Value from the tree

        *Return Type*
            Any valid python object

        @param key
            A TreeDict keychain.

            Type - string
        """
        with self._lock.reader:
            return self._read(key)

    def get_meta(self, key=None):
        """
        Allows for discovery of the underlying tree structure

        *Returns*
            A list of TreeDict keychains for all child nodes

        @param key
            When provided with a TreeDict keychain, it'll
            return all valid child nodes as full keychains.
            When no option is supplied, it'll return the
            root keychains.

            Type - string, Default = None
        """
        with self._lock.reader:
            return self._tree.get_keychain(keychain=key)

    def _write(self, key, value, safety=True):
        """
        An abstracted way to write to the tree; this way both
        a write call and a modify call can use the same code path.

        A write will always travers the tree to do a write, but will
        cache that result afterwards. Attempting to read the lookup
        cache to do a write is suprising complicated because root
        nodes *can* be leafs, and I don't want to deal with this
        problem right now.

        *NOT THREAD SAFE*
        """
        write_ok = False
        try:
            root, cache_attr = self._parse_key(key)
            if self._tree.is_leaf(key):
                self._tree.set_node(key, value=value)
                write_ok = True
                self._update_cache(cache_attr, cache_attr.replace('_', '.'))
                return write_ok
            else:
                raise ValueError('Attempted write would prune tree: Key=%s Value=%s' % (key, value))
        except KeyError:
            raise LookupError('Key does not exist in tree: %s' % key)

    def write(self, key, value, safety=True):
        """
        Write to the underlying data structure. Only one
        thread can write at a time. If a write is in flight
        all reads are blocked until the transaction is complete.

        @param key
            A TreeDict keychain

            Type - string

        @param value
            The value to write to the underlying data structure.

            Type - Any Python object

        @param safety
            Prevent accidental pruning of the tree.

            Type - boolean

            Example without safety::
              >>> ed = EvilDict({'a': {'b': {'c' : 1}}})
              >>> ed.read('a.b.c')
              >>> 1
              >>> ed.read('a')
              >>> {'b': {'c' : 1}}
              >>> ed.write('a', 'oops')
              >>> ed.read('a')
              >>> 'oops'
              >>> ed.read('a.b.c')
              Traceback (most recent call last):
                File "<stdin>", line 1, in <module>
                File "/home/willnx/git/evildict/evildict/evil.py", line 135, in read
                  return self._read(key)
                File "/home/willnx/git/evildict/evildict/evil.py", line 112, in _read
                  raise LookupError('Key does not exist in tree: %s' % key)       
              LookupError: Key does not exist in tree: no.key
        """
        with self._lock.writer:
            return self._write(key, value, safety=safety)

    def modify(self, key, value=None, condition=None, mutate=None, safety=True):
        """
        Performes a read and update process.
        Stacking a condition with a mutate function is permitted,
        just remember that with great power comes great responsibilty.

        @param key
            A TreeDict keychain

            Type - string

        @param value
            The value to write to a given key

            Type - Any Python object


        @param condition
            If condition is supplied, the write will only occur
            when the condition returns True. The frist argument to
            the function will always be what the value found for
            the supplied key.

            Type - Function

        @param mutate
            If mutate is supplied, then the value read for the supplied
            key will be passed as the frist argument, and the mutated
            value returned will be written to the underlying data set.
            Using this param negates anythign supplied via the 'value'
            param.

            Type - Function


        -Examples of using a condition-
        ::
          Write only if the value for the key is greater than 1
                lambda x: x > 1

          Taking scope into account
                st = time.time()
                ed = EvilDict(some_tree)
                ed.modify(some_key, value=10, condition=lambda x: x == st)

          Without hurting your brain over lambda functions
                def myfunc(x):
                    x != 0
                ed.modify(some_key, value=10, condiditon=myfunc)


        -Examples of mutating the write value-
        ::
          Increment a value
                lambda x: x + 1
               

        """
        with self._lock.writer:
            # with a write lock, you can read all day
            original_value = self._read(key)

            # See if we should write the value
            do_write = False
            write_ok = False
            if hasattr(condition, '__call__'):
                # only do write if function returns True
                if condition(original_value):
                    do_write = True
            else:
                do_write = True

            if mutate:
                write_out = mutate(original_value)
            else:
                write_out = value

            if do_write:
                write_ok = self._write(key, write_out, safety=safety)

            if do_write and write_ok:
                return True
            else:
                return False

    def _parse_key(self, key):
        """
        Both reads and writes have to parse a key in the same
        manner to allow the cache process to run when there is
        a cache miss.

        *Returns*
            list representation of the key and a pythonic
            version of the key (so can be made a class attribute
            for the caching system)

        *Return Types*
            Tuple -> (list, string)

        @param key
            The key to parse

            Type - string
        """
        root = key.split('.')
        # Handle root lookups
        if len(root) == 1:
            attr = key
        else:
            attr = '_'.join(root[:-1])
        return root, attr

    def _update_cache(self, attr, parent_key):
        """
        Called when there is a cache miss.

        @param attr
            The parent tree of the lookup.

            Type - string

        @param attr_val
            The subtree that gets cached.

            Type - Any python object
        """
        self._cache_meta.append(attr)
        attr_val = self._tree.get_node(parent_key)
        setattr(self, attr, attr_val)

        if len(self._cache_meta) > self._cache_max:
            pop_attr = self._cache_meta.popleft()
            if not (pop_attr in self._cache_meta):
                # no duplicate entries, so nuke it
                delattr(self, pop_attr)

if __name__ == '__main__':
    t = { 'one': {'two': {'three' : 3}}}
    ed = EvilDict(t)
    var = ed.read('one')
    print var
