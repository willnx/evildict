	#!/usr/bin/env python
	# -*- coding: UTF-8 -*-

__author__ = "Nicholas Willhite"
__version__ = 0.1

class TreeDict(dict):
    """
    A pyhton implamentation of a Tree data strutre.

    The goal of this object is to provide a convent (not performant)
    way to manipulate a nested dictionary.

    Within this object, a concept of a 'keychain' is used to describe
    a path through the hierarchy of parent to child nodes. Keychains
    are represented by comma delimited strings, where each element
    between a comma is a node within the tree structure.

    Example of a keychain, and resulting data structure:

        keychain          data structure
        'a.b.c'  ->  {'a' : {'b' : {'c' : VALUE}}}
    """

    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)

    def __getitem__(self, key):
        return dict.__getitem__(self, key)

    def __setitem__(self, key, val):
        dict.__setitem__(self, key, val)

    def __str__(self):
        return dict.__repr__(self)

    def __repr__(self):
        dictrepr = dict.__repr__(self)
        return '{0}({1})'.format(type(self).__name__, dictrepr)

    def update(self, *args, **kwargs):
        """
        The basic method Python uses to update a normal dictionary
        """
        for k, v in dict(*args, **kwargs).iteritems():
            self[k] = v

    def _recursive_search(self, node):
        """
        An abstraction layer for simple lookups in the TreeDict
        """
        return reduce(lambda d, k: d[k], node, self)

    def get_node(self, keychain, protect=True):
        """
        Returns a value from the TreeDict if the supplied list
        is a valid keychain.

        @param keychain
            A period delimited string representing a
            hierarchical path within the TreeDict.

            Type - string

        @param protect
            Returns a copy instead of pointers for
            mutable sequnce data types (dicts, lists, sets).
            Because TreeDict can return subtrees of itself
            it is entirly possible to accidently mutate data.
            You can also do really interesting things, thus
            the power to choose. Remember, with great power
            comes great responsibility.

            Type - Boolean, default = True 

        Example with protection
          >>> tree = TreeDict({'a':{'b':{'c': 1}}})
          >>> value = tree.get_node('a', protect=True)
          >>> print value
          {'b': {'c': 1}}
          >>> value['b'] = 1000
          >>> print tree
          {'a': {'b': {'c': 1}}}

        Example without protection
          >>> tree = TreeDict({'a':{'b':{'c': 1}}})
          >>> value = tree.get_node('a', protect=False)
          >>> print value
          {'b': {'c': 1}}
          >>> value['b'] = 1000
          >>> print tree
          {'a': {'b': 1000}}
        """
        try:
            tmp = self._recursive_search(keychain.split('.'))
            if protect:
                if type(tmp) == dict:
                    return dict(tmp)
                if type(tmp) == list:
                    return list(tmp)
                if type(tmp) == set:
                    return set(tmp)
                else:
                    return tmp
            else:
                return tmp
        except KeyError:
            raise KeyError('List of keys != valid keychain in TreeDict')

    def set_node(self, keychain, value=None, make=False, node=False):
        """
        The main method of inserting data into the TreeDict.

        @param keychain
            A period delimited string representing a
            hierarchical path within the TreeDict. 

            Type - string

        @param value
            The data to store in the leaf.

            Type - Any Python Object, Default = None

        @param make
            If the supplied keychain does not exist, create it.
            NOTE - Setting node to False induces two searches
            One to search and make the keychain, another
            set the value of the keychain.

            Type - Boolean

        @param node
            Only used when the 'make' param is set to True.
            Setting this value to True along with the 'make'
            param prevents two lookups by not converting the
            last node in the supplied keychain to a leaf.

            Type - Boolean
        """
        key_list = keychain.split('.')
        if make:
            if key_list[0] not in self.keys():
                self[key_list[0]] = {}

            for i in xrange(1, len(key_list) + 1):
                try:
                    tmp = self._recursive_search('.'.join(key_list[:i]))
                except (KeyError, TypeError):
                    self._recursive_search(key_list[:(i - 1)])[key_list[i - 1]] = {}

            if node is False:
                self._recursive_search(key_list[:-1])[key_list[-1]] = value

        else:
            if node is False:
                self._recursive_search(key_list[:-1])[key_list[-1]] = value
            else:
                self._recursive_search(key_list[:-1])[key_list[-1]] = {}

    def pop_node(self, keychain):
        """
        Remove a key value pair from a provided keychain

        @param keychain
            A period delimited string representing a
            hierarchical path within the TreeDict.

            Type - string

        Example:
          >>> tree = TreeDict({'one': {'two': {'a': 1, 'b' : 2}}})
          >>> keychain = 'one.two.a'
          >>> tree.pop_node(keychain)
          >>> print tree
          {'one': {'two': {'b' : 2}}}
        """
        key_list = keychain.split('.')
        try:
            self._recursive_search(key_list[:-1]).pop(key_list[-1])
        except TypeError:
            raise TypeError('Invalid keychain: %s' % str(keychain))

    def is_leaf(self, keychain):
        """
        Takes a keychain, and reports if the value found is a leaf
        or a node.

        *Returns*
            True if keychain value == leaf

        *Return Type*
            Boolean

        @param keychain
            A comma delimited string representing a
            hierarchical path within the TreeDict.
        """
        try:
            if type(keychain) == list:
                node = keychain
            else:
                node = keychain.split('.')

            value = self._recursive_search(node)
            if type(value) == dict:
                return False
            else:
                return True

        except KeyError:
            raise KeyError('Invalid keychain: %s' % str(keychain))

    def get_keychain(self, keychain=None):
        """
        Searches the TreeDict and returns a list of valid keychains
        from immedate child nodes.

        *Returns*
            Valid keychain elements
        *Return Type*
            List

        @param keychain
            The hierarchical path to start the search from.
            If no value is supplied, the child nodes from
            the root is returned.
        """
        if keychain is None:
            return self.keys()

        if type(keychain) == str:
            base_key = keychain
            subtree = self._recursive_search(keychain.split('.'))
        elif type(keychain) == list:
            base_key = ".".join(keychain)
            subtree = self._recursive_search(keychain)

        new_chains = []
        if type(subtree) == dict:
            for k, v in subtree.iteritems():
                if type(v) == dict:
                    new_chains.append("%s.%s.%s" % (base_key, k, ".".join(v.keys())))
                else:
                    new_chains.append("%s.%s" % (base_key, k))

        return new_chains
