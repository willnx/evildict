#!/usr/bin/env python
# -*- coding: UTF8 -*-

from threading import Lock, Condition, current_thread
from contextlib import contextmanager
from time import time

class ReadWriteLock(object):
    """
    A python implamentation of a Read/Write Lock
    (aka Shared Lock) which favors writers over readers.

    The goal of this object is to provide a simple way to
    ensure state when reading and writing to a data set via
    multiple threads with minial chance to deadlock. To meet
    this goal, the concept of lock promotion has been removed
    along with instrumenting timeouts for locks to be acquired.

    The timeouts are based off the system clock; this opens the
    door to unexpected behavior if the system clock is modified
    while the program is running. Implementing via sleeps would
    mean that the timeout value is a fuzzy value, where real
    timeout = supplied value + run time. An attempt to use the
    CPU time would be equally bad, as time spent sleeping does
    not count towards the total sum for eplased time.

    If you wish to disable timeouts, set the value to 'None'.
    Setting the value to 0 (zero) will cause almost every
    thread to raise a RuntimeError.

    @param read_timeout
        The number of seconds until a read operation raises a RuntimeError

        Type - int, default = 30

    @param write_timeout
        The number of seconds until a write operation raises a RuntimeError

        Type - int, default = 30
    """

    def __init__(self, read_timeout=30, write_timeout=30):
        self._condition = Condition(Lock())
        self._writer = None
        self._pending_writers = set()
        self.write_timeout = write_timeout
        self._readers = {}
        self.read_timeout = read_timeout

    @property
    @contextmanager
    def reader(self):
        """
        Allows use of the 'with' statement for read locks.
        Exiting the with statement releases the lock.

        -Example-
        ::
            with lock.reader:  <- obtains lock
                do stuff       <- does work
            new_var = 1        <- exited with statement, and released lock
        """
        self.acquire_read()
        yield
        self.release()

    @property
    @contextmanager
    def writer(self):
        """
        Allows use of the 'with' statement for write locks.
        Exiting the with statement releases the lock.

        -Example-
        ::
            with lock.writer:  <- obtains lock
                do stuff       <- does work
            new_var = 1        <- exited with statement, and released lock
        """
        self.acquire_write()
        yield
        self.release()

    def acquire_read(self):
        """
        The core method to obtain a read lock.
        Threads can obtain multiple read locks.
        A Thread with a write lock that calls for a read lock
        will raise a RuntimeError.

        All manually aquired read locks *MUST* have an associated
        release call. It is highly recommened to use the 'reader'
        method to obtain a read lock instead of this method.
        """
        if self.read_timeout is not None:
            doomsday = time() + self.read_timeout
        me = current_thread()

        with self._condition:
            if self._writer is me:
                # To perform a read/ write process
                return

            while True: 
                if (self._writer is None) and (len(self._pending_writers) <= 0):
                    self._readers[me] = self._readers.get(me, 0) + 1
                    # obtained a read lock
                    return

                if self.read_timeout is not None:
                    count_down = doomsday - time()
                    if count_down <= 0:
                        msg = 'Acquiring read lock timed out after %s seconds' % self.read_timeout
                        raise RuntimeError(msg)    
                    self._condition.wait(count_down)
                else:
                   self._condition.wait() 

    def acquire_write(self):
        """
        The core method to obtain a write lock.
        Attempting to take multiple write locks while a thread
        already owns the write lock will cause a RuntimeError
        to be raised.

        All manually aquired write locks *MUST* have an associated
        release call. It is highly recommened to use the 'writer'
        method to obtain a write lock instead of this method.
        """
        if self.write_timeout is not None:
            doomsday = time() + self.write_timeout
        me = current_thread()

        with self._condition:
            if self._writer is me:
                # You already have a write lock
                return

            while True:
                if (self._writer is None) and (len(self._readers) <= 0):
                    self._writer = me
                    # in case we were a pending writer
                    try:
                        self._pending_writers.remove(me)
                    except KeyError:
                        pass
                    # obtained a write lock
                    return

                else:
                    self._pending_writers.add(me)

                if self.read_timeout is not None:
                    count_down = doomsday - time()
                    if count_down <= 0:
                        msg = 'Acquiring write lock timed out after %s seconds' % self.read_timeout
                        raise RuntimeError(msg)
                    self._condition.wait(count_down)
                else:
                    self._condition.wait()

    def release(self):
        """
        Release the currently held lock.
        Calling release on a thread with no lock raises a ValueError.
        """
        me = current_thread()
        with self._condition:
            if self._writer is me:
                self._writer = None
                self._condition.notifyAll()

            elif me in self._readers:
                self._readers[me] -= 1
                if self._readers[me] <= 0:
                    # remove this thread b/c is has no more read locks
                    del self._readers[me]
                    if len(self._readers) <= 0:
                        # No more readers, notify waiters
                        self._condition.notifyAll()
            else:
                raise ValueError("Trying to release unheld lock")

