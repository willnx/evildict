.. EvilDict documentation master file, created by
   sphinx-quickstart on Tue Jun  9 23:20:04 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EvilDict's documentation!
====================================

EvilDict is an experiment, an evil one using Python dictionaries to
build something that you really shouldn't.

The idea behind this project is to see what intersting thing could be
built by levraging the fact that dictonaries in Python are mutable,
and mutable objects when requested from a dictionary simply returns
the pointers instead of a copy.

What's been created here, in short, is a thread safe caching tree data
structure that supports multiple readers, and a single writer. The goal
of this implamentation was to make this evil simple to use.

If you're asking yourself why? Why make this thing if it's evil? The
answer is simple; because it's freaking awesome! The truth is, that
like Frankensitein's monster, EvilDict isn't inhertently good or bad;
it is completely dependant on how it's used. For a POC, it's awesome
because it allows you to skip the implamentation of a proper backend
database system. Instead, you can just parse a xml or json file that
has a nested dictionary, feed that to EvilDict and bam! You have
a backend data struture to point something at. In a production
enviroment, it's likely to bring more pain than anything.

With great power comes great responsability.

Contents:

.. toctree::
   :maxdepth: 2

   evildict.rst
   modules.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

