evildict package
================

Submodules
----------

evildict.evil module
--------------------

.. automodule:: evildict.evil
    :members:
    :undoc-members:
    :show-inheritance:

evildict.superlocks module
--------------------------

.. automodule:: evildict.superlocks
    :members:
    :undoc-members:
    :show-inheritance:

evildict.treedict module
------------------------

.. automodule:: evildict.treedict
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: evildict
    :members:
    :undoc-members:
    :show-inheritance:
