#-*- coding: UTF-8 -*-

import unittest
from evildict import EvilDict

class TestEvil(unittest.TestCase):
    """
    Suite for testing methods of EvilDict.

    *Note* Because EvilDict is a singleton, we can only
    really test the cache via a seperate unittest suite.
    Simply deleteing the EvilDict object does not clear
    the cache.
    """

    def setUp(self):
        '''Runs before each test case'''
        t = {'a': { 'b' : { 'c' : { 'one' : { '1' : 1},
                                    'two' : { '2' : 2},
                                    'three' : { '3' : 3},
                                    'list' : [1,2,3,4],
                                    'tup' : (1,2,3,4),}}
                  },
              'z' : 'woot'}

        self.ed = EvilDict(t, timeout=10, cache_count=5)
        # uncomment to see the cache grown with each unittest
        # "More weird stuff from singletons in python"
        #
        #print len(self.ed._cache_meta)
        #methods = dir(self.ed)
        #methods = [x for x in methods if not x.startswith('_')]
        #print methods

    def tearDown(self):
        '''Runs after each test case: Delete EvilDict b/c it's a singleton'''
        del self.ed

    def test_read(self):
        """
        Basic test of reading from the EvilDict
        """
        self.assertEqual(self.ed.read('a.b.c.one.1'), 1)

    def test_get_meta_none(self):
        """
        Test of the 'get_meta' method with no defined keychain
        """
        expected = ['a', 'z']
        actual = self.ed.get_meta()
        msg = "\nExpected: %s\nActual: %s" % (expected, actual)

        TESTPASSED = True
        for i in actual:
            if i not in expected:
                TESTPASSED = False
        self.assertTrue(TESTPASSED, msg)

    def test_get_meta_with_keychain(self):
        """
        Test of the 'get_meta' method when providing a valid keychain
        """
        expected = ['a.b.c.tup', 'a.b.c.three.3', 'a.b.c.list',
                    'a.b.c.two.2', 'a.b.c.one.1']
        actual = self.ed.get_meta('a.b.c')
        msg = "\nExpected: %s\nActual: %s" % (expected, actual)

        TESTPASSED = True
        for i in actual:
            if i not in expected:
                TESTPASSED = False
        self.assertTrue(TESTPASSED, msg)

    def test_write(self):
        """
        Basic test of writing to the EvilDict
        """
        self.ed.write('a.b.c.one.1', 'one')
        self.assertEqual(self.ed.read('a.b.c.one.1'), 'one')

    def test_modify_with_condition_positive(self):
        """
        Test modifing the EvilDict when the 'condition' param is used,
        and the condition is positive.
        """
        self.ed.modify('z', value='new_val', condition=lambda x: True)
        self.assertEqual(self.ed.read('z'), 'new_val')

    def test_modify_with_condition_negitive(self):
        """
        Test modifing the EvilDict when the 'condition' param is used,
        and the condition is negitive.
        """
        self.ed.modify('z', value='new_val', condition=lambda x: False)
        self.assertEqual(self.ed.read('z'), 'woot')






if __name__ == '__main__':
    unittest.main()
