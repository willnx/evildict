#-*- coding: UTF-8 -*-

import unittest
from evildict import TreeDict

class TestTreeDict(unittest.TestCase):
    """
    A suite of unittests for the TreeDict object.
    Because TreeDict is subclassed from a regular Python dictionary,
    only the 'special sause' methods added are tested within this
    suite.
    """

    def setUp(self):
        '''Runs before each test'''
        t = {'a' : { 'b' : { 'c' : { 'f' : { 'leaf' : True },
                                     'list' : [1,2,3],
                                     'string' : 'woot',
                                     'set' : set([1,2,3]),
                                     'dict' : {'one': 1},
                                     'd' : 1}}}}
        self.tree = TreeDict(t)

    def test_set_node_make_false_failes(self):
        """
        Test making a leaf with no existing path
          This test should prove that the 'make' param is required
          when there isn't an existing path
        """
        self.assertRaises(KeyError, self.tree.set_node, 'a.b.x.y.z',
                                                        value=1,
                                                        make=False)

    def test_set_node_make_true_works(self):
        """
        Test making a leaf with no existing path
          Setting the 'make' param to True should make a path
        """
        self.tree.set_node('a.b.x.y.z', value=1, make=True)
        self.assertEqual(self.tree.get_node('a.b.x.y.z'), 1)

    def test_get_node_positive(self):
        """
        A positive test of the get_node method
        """
        self.assertEqual(self.tree.get_node('a.b.c.d'), 1)

    def test_is_leaf_when_leaf(self):
        """
        Test that the 'is_leaf' method returns True when the node
        is a leaf within the tree.
        """
        self.assertTrue(self.tree.is_leaf('a.b.c.d'))

    def test_is_leaf_when_node(self):
        """
        Test that the 'is_leaf' method return False when the node
        is *not* a leaf.
        """
        self.assertFalse(self.tree.is_leaf('a.b'))

    def test_pop_node_positive(self):
        """
        A positive test of the 'pop_node' method
        """
        val = self.tree.pop_node('a.b.c')
        self.assertEqual(len(self.tree.get_node('a.b')), 0)

    def test_pop_node_negative(self):
        """
        A negative test of the 'pop_node' method
        """
        self.assertRaises(KeyError, self.tree.pop_node, 'not.a.key')

    def test_get_keychain_from_root(self):
        """
        Test when no params are sent; should return root keychains
        """
        expected = ['a']
        self.assertEqual(self.tree.get_keychain(), expected)

    def test_get_keychain_from_node(self):
        """
        Ensure that child nodes are returned when passing in parent node
        """
        expected = ['a.b.c.f.leaf', 'a.b.c.list', 'a.b.c.string', 'a.b.c.set',
                    'a.b.c.d', 'a.b.c.dict.one']
        actual = self.tree.get_keychain(keychain='a.b.c')
        for i in actual:
            if i not in expected:
                self.assertTrue(False, '\nExpected: %s\nActual: %s' % (expected, actual))

    def test_get_keychain_from_leaf(self):
        """
        Should return an empty list
        """
        self.assertEqual(self.tree.get_keychain('a.b.c.d'), [])

    def test_get_node_negative(self):
        """
        A negative test of the 'get_node' method
        """
        self.assertRaises(KeyError, self.tree.get_node, 'not.a.key')

    def test_get_node_positive(self):
        """
        A positive test of the 'get_node' method
        """
        self.assertEqual(self.tree.get_node('a.b.c.d'), 1)

    def test_get_node_protect_off(self):
        """
        Test that protect = False will return pointers to mutable objects
        within the TreeDict.
        """
        satellite = self.tree.get_node('a.b', protect=False)
        satellite['c'] = 'updated'
        self.assertEqual(self.tree.get_node('a.b.c'), 'updated')

    def test_get_node_protect_off(self):
        """
        Test that protect = True will return a copy of the mutable
        object from the TreeDict.
        """
        satellite = self.tree.get_node('a.b', protect=True)
        satellite['c'] = 'updated'
        self.assertNotEqual(self.tree.get_node('a.b.c'), 'updated')


if __name__ == '__main__':
    unittest.main()
