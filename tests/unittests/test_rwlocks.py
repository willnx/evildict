#-*- coding: UTF-8 -*-

import unittest
from time import time, sleep
from threading import Thread

from evildict import ReadWriteLock

class TestReadWriteLock(unittest.TestCase):
    """
    """

    def setUp(self):
        """Runs before each test case"""
        self.lock = ReadWriteLock(read_timeout=10, write_timeout=10)

    def tearDown(self):
        """Runs after each test case"""
        del self.lock

    def thread_work(self, wait=None, lock_type=None, hold_for=None):
        """
        A utility function to simulate a thread doing work with a lock.

        @param wait
            How many seconds to before attempting to acquire a lock

            Type - int

        @param lock_type
            Acquire a Read or a Write lock.

            Type - string

        @param hold_for
            How many seconds to hold onto a lock

            Type - int
        """
        if wait:
            sleep(wait)

        if lock_type.lower() == 'read':
            with self.lock.reader:
                sleep(hold_for)
        elif lock_type.lower() == 'write':
            with self.lock.writer:
                sleep(hold_for)

    def pref_writers(self, val=None, lock_type=None, hold_for=None):
        """
        """
        if lock_type.lower() == 'write':
            with self.lock.writer:
                if val:
                    self.x += 1
                if hold_for:
                    sleep(hold_for)

        elif lock_type.lower() == 'read':
            with self.lock.reader:
                if val:
                   self.y = self.x
                if hold_for:
                    sleep(hold_for)

        else:
            raise RuntimeError('lock_type can only be read or write: supplied %s' % lock_type)



    def test_many_readers(self):
        """
        Ensure many readers can obtain a lock concurrently
        """
        start = time()
        threads = []
        for i in xrange(5):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'read',
                                                        'hold_for' : 1})
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()
        end = time()

        # If we serialized on reads, then the time it'll take for all the
        # threads to terminate would be just over 'hold_for' + thread count
        self.assertTrue((end - start) < 5)

    def test_many_writers(self):
        """
        Ensure we serialize writes
        """
        start = time()
        threads = []
        for i in xrange(5):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'write',
                                                        'hold_for' : 1})
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()
        end = time()

        # If we don't serialize writes, then the runtime will be less than
        # the number of threads + the 'hold_for' value
        self.assertTrue((end - start) >= 5, "Took only: %s seconds" % (end - start))

    def test_readers_block_writers(self):
        """
        Test that calls for a write lock are blocked if there are active read locks.
        """
        start = time()
        threads = []
        for i in xrange(1):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'read',
                                                        'hold_for' : 2})
            threads.append(t)
            t.start()

        for i in xrange(1):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'write',
                                                        'hold_for' : 1})
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()
        end = time()

        self.assertTrue((end - start) > 3)

    def test_writers_block_readers(self):
        """
        Test that calls for a read lock are blocked if there is an active write lock.
        """
        start = time()
        threads = []
        for i in xrange(1):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'write',
                                                        'hold_for' : 2})
            threads.append(t)
            t.start()



        for i in xrange(1):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'read',
                                                        'hold_for' : 1})
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()
        end = time()

        self.assertTrue((end - start) > 3)

    def test_writers_are_preferred(self):
        """
        If both a read lock and a new write lock are queued behind
        a thread that has an existing write lock, ensure that the
        new write lock will take presidence over the new read lock.
        """
        self.x = 2
        self.y = True

        threads = []
        # start 1st write
        for i in xrange(1):
            t = Thread(target=self.thread_work, kwargs={'wait': None,
                                                        'lock_type' : 'write',
                                                        'hold_for' : 1})
            threads.append(t)
            t.start()

        # start reader
        for i in xrange(1):
            t = Thread(target=self.pref_writers, kwargs={'lock_type' : 'read',
                                                         'val': self.y})
            threads.append(t)
            t.start()

        # start 2nd writer
        for i in xrange(1):
            t = Thread(target=self.pref_writers, kwargs={'lock_type' : 'write',
                                                         'val': self.x})
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()

        # If we allow the read to happend before the 2nd write, the value
        # of self.y will be 2
        self.assertEqual(self.y, 3)


if __name__ == '__main__':
    unittest.main()
