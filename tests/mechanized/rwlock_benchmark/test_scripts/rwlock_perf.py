# -*- coding:UTF-8 -*-

from random import randint
from time import sleep, time
from evildict.superlocks import ReadWriteLock

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class Transaction(object):
    def __init__(self):
        self.locker = ReadWriteLock()
        sleep(randint(0,3))

    def run(self):
        work_time = 0.1
        error = False
        write_triggers = (1,5,7)
        start = time()
        try:
            for i in xrange(10):
                if i in write_triggers:
                    # 30% write
                    with self.locker.writer:
                        sleep(work_time)

                else:
                    with self.locker.reader:
                        sleep(work_time)
        except RuntimeError:
            error = True

        latency = time() - start
        timer_header = 'ReadWriteLock Perf with {0}% writes'.format(len(write_triggers) * 10)
        self.custom_timers[timer_header] = latency

        assert (error == False)


if __name__ == '__main__':
    trans = Transaction()
    trans.run()
    print 'done'
