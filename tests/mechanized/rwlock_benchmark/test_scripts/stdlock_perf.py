# -*- coding:UTF-8 -*-

from random import randint
from time import sleep, time
from threading import Lock

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class Transaction(object):
    def __init__(self):
        self.locker = Lock()
        sleep(randint(0,3))

    def run(self):
        work_time = 0.1
        error = False
        start = time()
        try:
            for i in xrange(10):
                with self.locker:
                    sleep(work_time)
        except RuntimeError:
            error = True

        latency = time() - start
        self.custom_timers['Standard Lock Performance'] = latency

        assert (error == False)


if __name__ == '__main__':
    trans = Transaction()
    trans.run()
    print dir(trans)
