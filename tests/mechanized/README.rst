============================
Multi-mechanized based tests
============================

`Multi-Mechanized website <http://testutils.org/multi-mechanize>`_.

Tests within this directory utilize the Multi-Mechanize library.
On your system, configure the library, then simply run the suite.

Installing Multi-Mechanize
--------------------------

  $ sudo apt-get install python-pip python-matplotlib

  $ sudo pip install -U multi-mechanize

Running a benchmark suite
-------------------------
(where benchmark_suite is a directory within this directory)

  $ multimech-run benchmark_suite

