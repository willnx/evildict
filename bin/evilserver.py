#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import argparse

from flask import Flask, request, jsonify
from evildict import EvilDict


app = Flask(__name__)
ed = EvilDict({'a':{'b': {'c': 1}}})

@app.route('/evil', methods=['GET', 'POST'])
def muhahaha():

    key = request.args.get('key')
    value = request.args.get('value')
    meta = request.args.get('meta')

    if request.method == 'POST':
        return do_write(key, value)

    else:
        return do_read(key, meta)

@app.route('/')
def home():
    return "EvilDict is active!"


def do_read(key, meta):
    data = {'error' : 'MEGAERROR - please file bug'}
    try:
        if key and meta:
            data = ed.get_meta(key=key)
        elif meta and not key:
            data = ed.get_meta()
        elif key and not meta:
            data = ed.read(key)
        elif not (key and meta):
            data = {'error': 'must supply key (string) or meta (boolean) param'}
    except Exception as e:
        data = {'error' : '%s' % e.message} 

    return jsonify(data=data)

def do_write(key, value):
    data = {'error' : 'MEGAERROR - please file bug'}
    try:
        if not (key and value):
            data = {'error' : 'must supply the key and value params to write'}
        else:
            write_ok = ed.write(key, value)
            data = {'success' : write_ok}
    except Exception as e:
        data = {'error' : '%s' % e.message}

    return jsonify(data=data)


def _parse_cli():
    parser = argparse.ArgumentParser(description='Enables access to the EvilDict via HTTP')
    parser.add_argument('-i', '--ip', type=str,
                        help='The IP to run the HTTP server on. Default = 127.0.0.1')
    parser.add_argument('-p', '--port', type=int, default=5000,
                        help='The port to run the server on. Default = 5000')

    return parser.parse_args()

if __name__ == '__main__':
    args = _parse_cli()
    if args.ip is None:
        HOST = '127.0.0.1'
    else:
        HOST = args.ip
    PORT = args.port

    app.run(debug=True, host=HOST, port=PORT)
