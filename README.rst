================
EvilDict Package
================
EvilDict is an experiment to see what can be done with Python
dictionaries. What's been created is a thread safe, caching
tree data structure that supports many readers and a single writer.

With great power, comes great responsiblity.

Included Batteries
------------------
- A script to interact with EvilDict via HTTP
- Fancy benchmarking scripts
- Unittests

Dev install
-----------
To run all tests, and update code

    $ sudo apt-get install python-pip python-matplotlib

    $ sudo pip install -U multi-mechanize

    $ sudo python setup.py develop
